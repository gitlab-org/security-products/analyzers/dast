# GitLab DAST

GitLab tool for running Dynamic Application Security Testing (DAST) on provided
web site.

This repository is mirroring the docker images from https://gitlab.com/gitlab-org/security-products/dast.  
The latter will be migrated to this new location with https://gitlab.com/gitlab-org/gitlab/-/issues/215930 
